# Configuration

Description of sections and options for `Shot` ini file configuration.


## global

---

_*Neurosoft Microservice Template* inherited parameters_

```ini
[global]
bind.ip = 127.0.0.1
bind.port = 5543
log.file = stdout
static.files.root = /ncshot/page
config.presets = /ncshot/var/presets.json
```

**bind.ip** [string] `required` - The service's HTTP server will listen on this interface.
To use all interfaces available on the machine, type `0.0.0.0`

**bind.port** [int] `required` - HTTP server's listen port

**log.file** [string] `(default: stdout)` - File path or stdout, as the output of the application's logger

**static.files.root** [string] `(default: <empty>)` - If specified, the application will serve static files from this directory under the `/static` path
If the directory contains an `index.html` file, it will be served under `/`

This option, is usually pre-filled by installers to point to a simple testing web-interface which is included in the application's package

**config.presets** [string] `(default: <empty>)` - path to a JSON file containing the value of the `config-presets` optional parameter

## engine

---

_Parameters controlling the ANPR/MMR Engine initialization and behaviour_

```ini
[engine]
count = 16
dta.folder = /ncshot/res/dta
syntax.folder = /ncshot/res/syntax
enable.adr = 0
enable.mmr = 1
```

**dta.folder** [string] `required` - Path to directory containing necessary ANPR runtime files.

**syntax.folder** [string] `required` - Path to directory containing license plate syntax JSON definitions.

**count** [int] `(default: 0)` - Number of engines initialized for concurrent processing.
For best results, should be set to the number of cores available on the machine.
Value of 0 autodetects and uses all available cores on the host.

**enable.adr** [int] `(default: 1)` - Enable the ADR plate recognition engine.

**enable.mmr** [int] `(default: 1)` - Enable the Make & Model recognition engine.

Note: Enabling and disabling specific engines such as MMR can only be done on the application instance level, and not per-request.
To use `Shot` with MMR enabled or disabled in some cases, run two separate instances of the service.

## mmr

---

_Additional parameters for the MMR Engine_

```ini
[mmr]
data.file = /ncshot/data/dta/mmr2/mmr2dta.dta
patterns.file = /ncshot/data/dta/mmr2/mmr2patts.dta
plate.scales.dict.filename = /ncshot/data/dta/mmr2/plate-scales.json
veh.det.enabled = 1
```

**data.file** [string] `required` - Path to MMR neural net definitions

**patterns.file** [string] `required` - Path to MMR pattern definitions

**plate.scales.dict.filename** [string] `required` - Path to additional MMR metadata

**veh.det.enabled** [int] `(default: 1)` - If `engine/enable.mmr` is true, this flag enables the MMR Detector feature

## handler

---

_Optional parameters for the /recognize REST handler output_

```ini
[handler]
default.output.format = message
default.message.suffix = worker-instance-7
notification.urls = http://127.0.0.1:7002/events http://127.0.0.1:7004/message
```

**default.output.format** [string] `default: json` - One of: json, message. Defines the output format of the rest handler.

| Option   | Description                                                                         |
| -------- | ----------------------------------------------------------------------------------- |
| json     | JSON document containing the list of recognition results                            |
| message  | TAR file in the `VehicleTraces` format, with optional resources such as plate crops |

**default.message.suffix** [string] `default: <empty>` - Optional suffix for the `VehicleTraces` message ID

**notification.urls** [list of strings] `default: <empty>` - list of URLs to notify with ``VehicleTraces`` messages after each recognized image. If this value is **NOT** empty, the handler with respond with just the ``VehicleTraces`` message ID.

## message

---

_Options for the TAR response message contents_

The entire section is optional and can be ommitted even if `handler/default.output.format` is selected as `message`.

```ini
[message]
resources = plate,mmr,adr
plate.crop.expand = 1.5,1.5
plate.crop.size = 150,50
mmr.crop.size = 320,220
```

**resources** [list of string] `default: plate,mmr,adr` - Any of: `laneview, thumbnail, plate, mmr, adr`

| Option    | Description                                                             |
| --------- | ----------------------------------------------------------------------- |
| laneview  | Original input image                                                    |
| thumbnail | Thumbnail of input image                                                |
| plate     | Cropped licesne plates, if any were recognized                          |
| mmr       | Cropped MMR regions, if MMR is enabled and any vehicles were classified |
| adr       | Cropped ADR plates, if ADR is enabled and any ADR plates were detected  |

**image.quality** [int] `default: 40` - JPEG compression image quality for the original image

**thumbnail.downscale** [int] `default: 4` - the original image's thumbnail is downscaled by this factor

**plate.crop.expand** [pair of float] `default: 1.3,1.2` - ratio by which the plate's quadrangle is inflated before cropping

**plate.crop.size** [pair of int] `default: 150,40` - size of cropped license plate image, in pixels

**adr.crop.expand** [pair of float] `default: 1.2,1.2` - ratio by which the ADR plate's quadrangle is inflated before cropping

**adr.crop.size** [pair of int] `default: 150,120` - size of cropped ADR plate image, in pixels

**mmr.crop.expand** [pair of float] `default: 1.0,1.0` - ratio by which the MMR ROI quadrangle is inflated before cropping

**mmr.crop.size** [pair of int] `default: 300,200` - size of cropped MMR ROI image, in pixels

## canard

---

_CANARD-specific options_

```ini
[canard]
evidence.root = /some/directory
```

**evidence.root** [string] `default: <empty>` - Path to a directory containing evidence images to be processed by the CANARD SOAP interface.

The SOAP interface is enabled only if this option is specified.